document.getElementById("b1").addEventListener("click", function () {

    var fahr = document.getElementById("f1").value;
    var cel = document.getElementById("f2").value;

    convertAll(fahr, cel);
});


function convertAll(fahr, cel) {
    fahr = parseFloat(fahr);
    cel = parseFloat(cel);
    document.getElementById("out1").innerHTML = ((fahr - 32) / 1.8).toFixed(2);
    document.getElementById("out2").innerHTML = ((cel * 9 / 5) + 32).toFixed(2);
}