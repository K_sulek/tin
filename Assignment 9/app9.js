"use strict";


const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const calculations = require("./calculations.js");
const app = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static("public"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", function(req, res) {
    res.render("home", {});
});



app.post("/calculate", function(req, res) {
    let result = calculations.calc(req.body);
    let data = calculations.format(req.body, result);
    res.setHeader('Content-Type', 'TIN_9/json');
    res.json(data);
});

app.listen(8080);
