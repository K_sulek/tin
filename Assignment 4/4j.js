function binSearch(arr, value) {
    let start = 0,
        end = arr.length - 1,
        mid;
    while (~arr.indexOf(value)) {
        mid = Math.ceil((start + end) / 2);
        if (value < arr[mid]) {
            end = mid - 1;
        } else if (value > arr[mid]) {
            start = mid + 1;
        } else {
            break;
        }
    }
    return mid || null;
}
const arr = [1, 7, 15, 21, 35, 44, 52, 100];
console.log(binSearch(arr, 44)); // 5
console.log(binSearch(arr, 23)); // null
