function Second_Greatest_Lowest(arr_num) {
  arr_num.sort(function(x,y) {
    return x-y;
  });

  var uniq = [arr_num[0]];
  var result = [];

  for(var j=1; j < arr_num.length; j++) {
    if(arr_num[j-1] !== arr_num[j]) {
      uniq.push(arr_num[j]);
    }
  }

  result.push(uniq[1],uniq[uniq.length-2]);
  return result.join(',');

}

console.log(Second_Greatest_Lowest([1,1,1,1,2,13,14,5,10]));
