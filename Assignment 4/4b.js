function fibo(n) {
var x = 0, y = 1, curr;

while (n >= 0){
    curr = x;
    x = x + y;
    y = curr;
    n--;
  }

  return y;
}

console.log("100th in fibo sequence is  " + fibo(100))
