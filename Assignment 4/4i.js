function toChange(amount, coins) {
    let change = [],
        count = 0,
        sum = 0;
    while (sum != amount && !(amount < 0)) {

        change.push(coins[count]);
        sum = change.reduce((a, b) => a + b);

        if (sum > amount) {
            change.pop();
            count++;
        }
    }
    return change;
}
const coins = [25, 10, 5, 2, 1];
console.log(toChange(66, coins)); // [25, 25, 10, 5, 1]
console.log(toChange(34, coins)); // [25, 5, 2, 2]
console.log(toChange(43, coins)); // [25, 10, 5, 2, 1]
console.log(toChange(-22, coins)); // []
