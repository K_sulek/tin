/*
* App working on:
* http://localhost:8080/
*/

"use strict";
var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.set("view engine", "ejs");
app.set("views", "./views");
app.use(express.static("public"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//8a
app.get("/hello", function (req, res) {
  res.send("hello world")
})

//8b
app.get("/form", function (req, res) {
    res.render("form", { formTitle: "someForm" })
  })

//8c
app.get("/formdata", function (req, res) {
    let body = req.body;
   res.render("formresult", { name: body.name, surname: body.surname, sID: body.sID });
       })

app.post("/jsondata", function(req, res) {
    res.render("formresult", {data: req.body});
});

app.listen(8080, () => console.log("Listening on port 8080 !"));