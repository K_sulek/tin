/*
Acces the server using: http://localhost:8080/sub?var1=20&var2=15
format : /operation ? variable1 & variable2
possible operations : add, sub, div, mul
*/
var http = require('http');
var url = require('url');


var server = http.createServer((request, res) => {
  var url_parts = url.parse(request.url, true);
  var query = url_parts.query;

  var number1 = parseInt(query.var1);
  var number2 = parseInt(query.var2);

  if(check(number1, number2) === true) {
    var result = operator(request.url, number1, number2);
    res.write(result + '');
    res.end();
  } else {
    res.write('wrong input');
    res.end();
  }

  

});

server.listen(8080);

console.log("server is running");


function operator(url_in, var1, var2) {

  if (url_in.includes("add")) {
    return var1 + var2;
  }
  if (url_in.includes("sub")) {
    return var1 - var2;
  }
  if (url_in.includes("div")) {
    return var1 / var2;
  }
  if (url_in.includes("mul")) {
    return var1 * var2;
  }
}

function check (el1, el2) {
  if(isNaN(el1) || isNaN(el2)) {
    return false;
  } else {
    return true;
  }
}