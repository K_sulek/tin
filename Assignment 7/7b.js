/*
To run this file input into console node 7b.js /filepath/...
*/

var fs = require('fs');
var path = process.argv[2];


fs.watch(path, function (event, filename) {
    console.log('\n')
    console.log(filename + ' event is: ' + event);
});