class Student {
    constructor(id, name, surname, grades) {
        
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.grades = grades;

    }

    studentStatus() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return "id: " + this.id + " name: " + this.name + " " + this.surname + " avg: " + avg;
    };

    get fullName() {
        return this.name + ' ' + this.surname;
    }

    set fullName(name) {
        var words = name.split(' ');
        this.name = words[0] || '';
        this.surname = words[1] || '';
    }

    get AvgGrade() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return avg;
    }
}

var someStudent = new Student(42, 'Arthur', 'Dent', [5,2,3,5,]);

console.log(someStudent.studentStatus());
someStudent.fullName = 'James Bond';
console.log(someStudent.fullName);
console.log(someStudent.AvgGrade);