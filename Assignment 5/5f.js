class Person {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }

    get fullName() {
        return this.name + ' ' + this.surname;
    }

    set fullName(name) {
        var words = name.split(' ');
        this.name = words[0] || '';
        this.surname = words[1] || '';
    }
}

class Student extends Person {
    constructor(name, surname, id, grades) {
        super(name, surname);
        this.id = id;
        this.grades = grades;
      }

      
    studentStatus() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return "id: " + this.id + " name: " + this.name + " " + this.surname + " avg: " + avg;
    };


    get AvgGrade() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return avg;
    }
}

var somePerson = new Person('Luke', 'Skywalker');
var someStudent = new Student('Han', 'Solo', 15234, [2,3,1,4,5]);

console.log(somePerson);
somePerson.fullName = 'Darth Vader';
console.log(somePerson);
console.log(somePerson.fullName);
console.log(someStudent.studentStatus());
console.log(someStudent.AvgGrade)


