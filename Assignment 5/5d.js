var id = 0;
function Student(first, last, gradeArr) {
    this.id = ++id;
    this.name = first;
    this.surname = last;
    this.grades = gradeArr;
    
    this.studentStatus = function() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return "id: s" + this.id + " name: " + this.name + " " + this.surname + " avg: " + avg;
    };
}

var someStudent = new Student("Krzysztof", "Sułek", [1, 5, 2, 3, 4]);

console.log(someStudent.studentStatus());

Object.defineProperty(someStudent, 'fullName', {
    get: function() {
        return this.firstName + ' ' + this.lastName;
    },
    set: function(name) {
        var words = name.split(' ');
        this.firstName = words[0] || '';
        this.lastName = words[1] || '';
    }
});

Object.defineProperty(someStudent, 'avgGrade', {
    get: function() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return avg;
    }
});
   
someStudent.fullName = "James Bond";
console.log(someStudent.fullName);
console.log(someStudent.avgGrade);