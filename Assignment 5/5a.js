var person = { 
  name: 'Krzysztof',
  surname: 'Sułek',
  age: 21,
  fullName: function() {
    return this.name + ' ' + this.surname;
},

dateOfBirth: function() {
    return (new Date()).getFullYear() - this.age;
}};

function showPerson() {
for (var x in person) {
console.log(person[x] + " is of type --> " + typeof person[x]);
}
}

console.log(person.fullName());
console.log(person.dateOfBirth());
showPerson(person);