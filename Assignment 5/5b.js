var id = 0;
function Student(first, last, gradeArr) {
    this.id = ++id;
    this.name = first;
    this.surname = last;
    this.grades = gradeArr;
    
    this.studentStatus = function() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return "id: s" + this.id + " name: " + this.name + " " + this.surname + " avg: " + avg;
    };
}

var someStudent = new Student("Krzysztof", "Sułek", [4, 2, 4, 2]);

console.log(someStudent.studentStatus());